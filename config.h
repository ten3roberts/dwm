/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 16;       /* snap pixel */
static const unsigned int gappx     = 2;
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "RobotoMono Nerd Font:size=10:antialias=true" };
static const char dmenufont[]       = "monospace:size=10";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

static char termcol0[] = "#000000"; /* black   */
static char termcol1[] = "#ff0000"; /* red     */
static char termcol2[] = "#33ff00"; /* green   */
static char termcol3[] = "#ff0099"; /* yellow  */
static char termcol4[] = "#0066ff"; /* blue    */
static char termcol5[] = "#cc00ff"; /* magenta */
static char termcol6[] = "#00ffff"; /* cyan    */
static char termcol7[] = "#d0d0d0"; /* white   */
static char termcol8[]  = "#808080"; /* black   */
static char termcol9[]  = "#ff0000"; /* red     */
static char termcol10[] = "#33ff00"; /* green   */
static char termcol11[] = "#ff0099"; /* yellow  */
static char termcol12[] = "#0066ff"; /* blue    */
static char termcol13[] = "#cc00ff"; /* magenta */
static char termcol14[] = "#00ffff"; /* cyan    */
static char termcol15[] = "#ffffff"; /* white   */
static char *termcolor[] = {
  termcol0,
  termcol1,
  termcol2,
  termcol3,
  termcol4,
  termcol5,
  termcol6,
  termcol7,
  termcol8,
  termcol9,
  termcol10,
  termcol11,
  termcol12,
  termcol13,
  termcol14,
  termcol15,
};

/* tagging */
static const char *tags[] = { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000" };

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp",        NULL, NULL, 0,      1, -1 },
    { "Firefox",     NULL, NULL, 1 << 1, 0, -1 },
    { "Pavucontrol", NULL, NULL, 0,      1, -1 },
    { "Thunar",      NULL, NULL, 0,      1, -1 },
    { "Scratchpad",  NULL, NULL, 0,      1, -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
#include "gaplessgrid.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",    tile },        /* first entry is default */
    { "[M]",    monocle },
    /* { "[@]", spiral },      */
    { "[\\]",   dwindle },
    { "###",    gaplessgrid },
    { "><>",    NULL },        /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
{ MOD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
{ MOD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
{ MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \
{ MOD, XK_q,     ACTION##stack, {.i = 0 } }, \
{ MOD, XK_a,     ACTION##stack, {.i = 1 } }, \
{ MOD, XK_z,     ACTION##stack, {.i = 2 } }, \
{ MOD, XK_x,     ACTION##stack, {.i = -1 } },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

static const char *roficmd[] = { "rofi", "-show", "combi", NULL};
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };


static const char *mutecmd[] =    { "volctrl.sh", "toggle", NULL };
static const char *volupcmd[] =   { "volctrl.sh", "up", NULL };
static const char *voldowncmd[] = { "volctrl.sh", "down", NULL };

static const char *playpausecmd[] = { "playerctl", "play-pause", NULL };
static const char *playnextcmd[] =  { "playerctl", "next",       NULL };
static const char *playprevcmd[] =  { "playerctl", "previous",   NULL };
static const char *playstopcmd[] =  { "playerctl", "stop",       NULL };

static const char *brupcmd[] =   { "xbacklight", "-inc", "5", NULL };
static const char *brdowncmd[] = { "xbacklight", "-dec", "5", NULL };

static const char *termcmd[]    = { "st",               NULL };
static const char *termhere[]   = { "terminal_here.sh", NULL };
static const char *browsercmd[] = { "firefox",          NULL };
static const char *filecmd[]    = { "thunar",           NULL };
static const char *spotifycmd[] = { "switch_app",       "spotify", NULL };
static const char *discordcmd[] = { "switch_app",       "discord", "Discord", NULL };

static const char *scrot_screencmd[] = { "screenshot.sh", "screen" };
static const char *scrot_regioncmd[] = { "screenshot.sh", "region" };

static Key keys[] = {
    /* modifier                     key        function        argument */
    { MODKEY,                         XK_F5,                   xrdb,           { .v  = NULL     } },
    { MODKEY,                         XK_p,                    spawn,          { .v  = roficmd  } },
    { MODKEY,                         XK_Return,               spawn,          { .v  = termcmd  } },
    { Mod1Mask|ControlMask,          XK_r,                    spawn,          { .v  = termhere } },

    /* Programs                                                         */
    { MODKEY|ControlMask,             XK_f,                    spawn,          {.v = browsercmd } },
    { MODKEY,                         XK_e,                    spawn,          {.v = filecmd } },
    { MODKEY|ControlMask,             XK_s,                    spawn,          {.v = spotifycmd } },
    { MODKEY|ControlMask,             XK_d,                    spawn,          {.v = discordcmd } },

    { 0,                              XK_Print,                spawn,          {.v = scrot_screencmd }},
    { MODKEY|ShiftMask,               XK_s,                    spawn,          {.v = scrot_regioncmd }},

    /* Multimedia */
    { 0,                              XF86XK_AudioMute,        spawn,          {.v = mutecmd } },
    { 0,                              XF86XK_AudioLowerVolume, spawn,          {.v = voldowncmd } },
    { 0,                              XF86XK_AudioRaiseVolume, spawn,          {.v = volupcmd } },
    { MODKEY|ShiftMask,               XK_Right,                spawn,          {.v = brupcmd} },
    { MODKEY|ShiftMask,               XK_Left,                 spawn,          {.v = brdowncmd} },

    { 0,                              XF86XK_AudioPlay,        spawn,          {.v = playpausecmd} },
    { 0,                              XF86XK_AudioNext,        spawn,          {.v = playnextcmd} },
    { 0,                              XF86XK_AudioPrev,        spawn,          {.v = playprevcmd} },
    { 0,                              XF86XK_AudioStop,        spawn,          {.v = playstopcmd} },

    { MODKEY|ShiftMask,               XK_b,                    togglebar,      { 0}  },

    { MODKEY,                         XK_i,                    incnmaster,     { .i  = +1           } },
    { MODKEY,                         XK_d,                    incnmaster,     { .i  = -1           } },

    { MODKEY,                         XK_h,                    setmfact,       { .f  = -0.05}       },
    { MODKEY,                         XK_l,                    setmfact,       { .f  = +0.05}       },

    { MODKEY,                         XK_BackSpace,            zoom,           { 0}  },
    { MODKEY,                         XK_Tab,                  view,           { 0}  },
    { MODKEY|ShiftMask,               XK_w,                    killclient,     { 0}  },


    { MODKEY|ShiftMask,               XK_h,                    inplacerotate,  {.i = +1} },
    { MODKEY|ShiftMask,               XK_l,                    inplacerotate,  {.i = -1} },

    { MODKEY|ShiftMask,               XK_r,                    inplacerotate,  {.i = +2} },
    { MODKEY,                         XK_r,                    inplacerotate,  {.i = -2} },

    { MODKEY,                         XK_t,                    setlayout,      { .v  = &layouts[0]} },   /* Tall */
    { MODKEY,                         XK_m,                    setlayout,      { .v  = &layouts[1]} },   /* Monocle */
    { MODKEY,                         XK_f,                    setlayout,      { .v  = &layouts[2]} },   /* Spiral */
    { MODKEY,                         XK_g,                    setlayout,      { .v  = &layouts[3]} },   /* Grid */
    { MODKEY|ShiftMask,               XK_f,                    setlayout,      { .v  = &layouts[4]} },   /* Floating */
    { MODKEY,                         XK_space,                setlayout,      { 0}  },
    { MODKEY|ShiftMask,               XK_t,                    togglefloating, { 0}  },

    { MODKEY,                         XK_0,                    view,           { .ui = ~0           } },
    { MODKEY|ShiftMask,               XK_0,                    tag,            { .ui = ~0           } },

    { MODKEY,                         XK_comma,                focusmon,       { .i  = -1           } },
    { MODKEY,                         XK_period,               focusmon,       { .i  = +1           } },

    { MODKEY|ShiftMask,               XK_comma,                tagmon,         { .i  = -1           } },
    { MODKEY|ShiftMask,               XK_period,               tagmon,         { .i  = +1           } },

    { MODKEY|ShiftMask,               XK_Escape,               quit,           {0} },

    STACKKEYS(MODKEY,                 focus)
    STACKKEYS(MODKEY|ShiftMask,       push)

    TAGKEYS(                    XK_1, 0)
    TAGKEYS(                    XK_2, 1)
    TAGKEYS(                    XK_3, 2)
    TAGKEYS(                    XK_4, 3)
    TAGKEYS(                    XK_5, 4)
    TAGKEYS(                    XK_6, 5)
    TAGKEYS(                    XK_7, 6)
    TAGKEYS(                    XK_8, 7)
    TAGKEYS(                    XK_9, 8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

