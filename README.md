# Dwm

This is my personal dwm build and includes some various patches and keybinds

## Applied patches
| name             | descritption                                  |
| ---------------- | --------------------------------------------- |
| xrdb             | Pywal and xrdb integration                    |
| status2D + xrdb  | Colored status bar                            |
| inplacerotate    | Rotate through clients                        |
| focusonnetactive | Make wmctrl and rofi bring window to focus    |
| desktopfile      | Make dwm appear in display manager            |
| statusallmon     | Shows statusbar on all monitors               |
| stacker          | Manipulate stack, move, focus, swap, and push |
| fibonacci        | Fibonacci layout                              |

